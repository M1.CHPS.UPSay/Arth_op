/*
!         KARATSUBA.CPP
!====================================================================
!
!History  T BOUBEHZIZ 
!+        28/12/2016
!+        
!+        Karatsuba multiplication. 
!
!====================================================================
*/

#include <cmath>
#include <vector>
#include <string.h>
#include <string>
#include "OpNum.h"
#include <iostream>
#include "karatsuba.h"
#include "OpNum.h"

using namespace std;


HighNbr karatsuba(HighNbr x, HighNbr y){
		
	if (x.dim < 2 || y.dim<2) return x*y;

	int n=(x.dim/2)+(x.dim%2);
	
//-----------------------------------------------		
// take the largest amongs of the two numbers
//-----------------------------------------------	
    if(x.dim<y.dim){
	    n = (y.dim/2) + (y.dim%2);
	 //	val2.sig=-1;
	}
	cout<<"n " << n<<"\n";
	
//-----------------------------------------------
	
	// x = xa *b +xb
	// y = ya *b +yb
    int xdim=x.dim;
	int ydim=y.dim;

	x.dim=n;//xa
	y.dim=n;//ya
		
	auto u=karatsuba(x,y);
		
	x.dim=xdim;
	
	
	return u;

/*	
    HighNbr w=karatsuba(p,q);
	HighNbr v=karatsuba(q-p,s-r);

    q.dim=y.dim-n;
	q=subchar(y,n,y.dim);	

	//solution assambly
	if(v.sig==-1){
		return Mulbase(u,n)+Mulbase(u+w+v,n/2+(n%2))+w;
	}
	else{
	    return Mulbase(u,n)+Mulbase(u+w-v,n/2+(n%2))+w;
	}
*/
}
