/*
!         OPNUM.CPP
!====================================================================
!
!History  Y 
!+        31/12/2016
!+        
!+        Added addition, substraction operators to HighNbr type. 

!History  T BOUBEHZIZ 
!+        03/01/2017
!+        
!+        Overload operators 
!
!History  T BOUBEHZIZ 
!+        08/01/2017
!+        
!+        Adapt +operator to 256 computing 
!
!
!History  T BOUBEHZIZ 
!+        12/01/2017
!+        
!+        Memory optimization for +,-,* operator. 
!
!====================================================================
*/

#include <iostream>
#include <string.h>
#include "toolsNum.h"
#include "OpNum.h"

using namespace std;


//--------------- overload Addition -------------------------------------

HighNbr operator+(const HighNbr & val1,const HighNbr & val2){
        int dimMax,dimMin,basis=256, *tranz;
		
		// find the biger and the smaller one.
        HighNbr nbrMax=MaxDim(val1,val2);
        HighNbr nbrMin=MinDim(val1,val2);
		
        HighNbr reslt;
        reslt.dim=nbrMax.dim;
        reslt.tab=(int*)calloc(reslt.dim, sizeof(int));
		
        for(int i=0;i<nbrMin.dim;i++){
                reslt.tab[i]=reslt.tab[i]+nbrMax.tab[i]+nbrMin.tab[i];
				
            //-------------------------------------------------
     		//	managment retained number 			
            //-------------------------------------------------			
                if (reslt.tab[i] >= basis){
					if (i==nbrMin.dim-1 && nbrMin.dim==nbrMax.dim){
						reslt.dim=i+2;
						reslt.tab=(int*)realloc(reslt.tab, sizeof(int)*reslt.dim);
						}
				   reslt.tab[i+1]=reslt.tab[i]/basis;
                   reslt.tab[i]=reslt.tab[i]%basis; // le reste   
                }
            //--------------------------------------------------
        }
   //---------------------------------------------------
		if (nbrMax.dim>nbrMin.dim){
             for(int i=nbrMin.dim;i<nbrMax.dim;i++)	{		
			     reslt.tab[i]=reslt.tab[i]+nbrMax.tab[i];
                 //  managment retained number 				
                if (reslt.tab[i] >= basis){
					if (i==nbrMax.dim-1){
						reslt.dim=i+2;
						reslt.tab=(int*)realloc(reslt.tab, sizeof(int)*reslt.dim);
						}
				   reslt.tab[i+1]=reslt.tab[i]/basis;
                   reslt.tab[i]=reslt.tab[i]%basis; // le reste
                   }
				   
				}
   //--------------------------------------------------
		}
        free(nbrMax.tab); free(nbrMin.tab);		
        return reslt;
}




//--------------- soustraction -------------------------------------

HighNbr operator-(const HighNbr & val1, const HighNbr & val2){
        int basis=256;
        int tab1=0;
		
        HighNbr reslt;
		// find the biger one and the smaller one.
        HighNbr nbrMax=MaxDim(val1,val2);
        HighNbr nbrMin=MinDim(val1,val2);
		
		// create the resutl variable.
        reslt.dim=nbrMax.dim;
        reslt.tab=(int*)calloc(reslt.dim, sizeof(int));//new int[reslt.dim];
        reslt.sig=nbrMax.sig;
		
		// substraction the smaller from the biger number, in two stages : 
		// according to the dimension of the smaller 
        for(int i=0;i<nbrMin.dim;i++){ 
                reslt.tab[i]=nbrMax.tab[i]-(nbrMin.tab[i]+tab1);
				tab1=0;
				// if the difference is <0.
                if (reslt.tab[i] < 0){
                   reslt.tab[i]=nbrMax.tab[i]+basis-(nbrMin.tab[i]+tab1);
                   tab1=1; // passe a retained number to the next block.
                   }
        }
		free(nbrMin.tab);

        for(int i=nbrMin.dim;i<nbrMax.dim;i++){
        	reslt.tab[i]=nbrMax.tab[i]-tab1;
			tab1=0;
            if (reslt.tab[i] < 0){
                reslt.tab[i]=nbrMax.tab[i]+basis-(nbrMin.tab[i]+tab1);
                tab1=1; // passe a retained number to the next block.
            }			
        }
		free(nbrMax.tab);
        int i=reslt.dim-1;
        while (reslt.tab[i]==0 && i>0){
			//cout<<"substration i"<<i<<"\n";
		   i--;
		   reslt.dim--;
	    }
		
		reslt.tab=(int*)realloc(reslt.tab, sizeof(int)*reslt.dim);//new int[reslt.dim];
		
        return reslt;
    }

//--------------- Multiplication -------------------------------------

HighNbr operator*(const HighNbr & val1,const HighNbr & val2){
int basis=256, retain=0;
// ------------- initialisation -------------------------		
        HighNbr reslt;
		
		HighNbr nbrMax=MaxDim(val1,val2);
        HighNbr nbrMin=MinDim(val1,val2);
		
        reslt.dim=nbrMax.dim;
        reslt.tab=(int*)calloc(reslt.dim, sizeof(int));
//-------------------------------------------------------

    for(int i=0;i<nbrMin.dim;i++){
        for(int j=0;j<nbrMax.dim;j++){
			//----------------------------------------------
            reslt.tab[i+j]=reslt.tab[i+j]+nbrMin.tab[i]*(retain+nbrMax.tab[j]); //calcul du produit
			retain=0;
		    if(reslt.tab[j+i]>=basis){
				// extend reslt.tab in the case : --------------
			    if(i+j==reslt.dim-1){ 

				    reslt.dim=i+j+2;		
                    reslt.tab=(int*)realloc(reslt.tab, sizeof(int)*reslt.dim); //new int [i+j+2];
					//------------------------------------------
                    for(int k=reslt.dim;k<i+j+2;k++) reslt.tab[k]=0;
                    reslt.tab[i+j+1]=reslt.tab[j+i]/basis;					
			    }
				else{
				//----------------------------------------------
			        retain=reslt.tab[j+i]/basis;
				}
                reslt.tab[i+j]=reslt.tab[i+j]%basis;
				//cout<< "reslt "<< reslt.tab[i+j]<<"\n";
		    }
        }
    }
    free(nbrMax.tab);
	free(nbrMin.tab);
    return reslt;
}


