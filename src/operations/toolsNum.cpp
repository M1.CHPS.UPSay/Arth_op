/*
!         TOOLSNUM.CPP
!====================================================================
!
!History  T BOUBEHZIZ
!+        03/01/2017
!+        
!+        Put manipulation functions in separate file.
!
!====================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "OpNum.h"
#include "toolsNum.h"
	
//============================================================
// ------------------------ Tools ----------------------------
		

//----------------------- Dimension -------------------------- 	

HighNbr MaxDim(HighNbr val1,HighNbr val2){
	   
       if (val1.dim>val2.dim)
                 return val1;
       else if(val1.dim<val2.dim){
	//	         val2.sig=-1;
    	   	   	 return val2;
		}
       else{
    	   for(int i=val1.dim-1;i>=0;i--){
    		    if(val1.tab[i]>val2.tab[i])
    				return val1;				
			    else if(val2.tab[i]>val1.tab[i]){
//				   val2.sig=-1;
    			   return val2;
				}
    	    }
        }
    }
HighNbr MinDim(HighNbr val1,HighNbr val2){
	   
       if (val1.dim>val2.dim)
                 return val2;
       else if(val1.dim<val2.dim)
    	   	   	 return val1;
       else{
 	        for(int i=val1.dim-1;i>=0;i--){
 		         if(val1.tab[i]>val2.tab[i])
 			            return val2;
 		         else if(val2.tab[i]>val1.tab[i])
 			            return val1;
 	        }
        }
    }
	

	
	
	
	
