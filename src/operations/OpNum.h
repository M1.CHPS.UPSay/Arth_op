/*
!         OPNUM.H
!====================================================================
!
!History  Y 
!+        31/12/2016
!+        
!+        Added addition, substraction operators to HighNbr type. 

!History  T BOUBEHZIZ 
!+        31/12/2016
!+        
!+        Added a speicial Multiplication function for karatsuba method.

!History  T BOUBEHZIZ 
!+        03/01/2017
!+        
!+        Overload operators 
!
!====================================================================
*/

#ifndef LONGNOMBRE_H_
#define LONGNOMBRE_H_

#include <functional>
using namespace std;


typedef struct HighNbr HighNbr;

struct HighNbr{
   int *tab;
   int  dim,head=0;
   int sig=1;
};


HighNbr operator+(const HighNbr & val1,const HighNbr & val2);
HighNbr operator-(const HighNbr & val1,const HighNbr & val2);
HighNbr operator*(const HighNbr & val1,const HighNbr & val2);

//--------------- Multiplication base --------------------------------
/*auto Mulbase=[](HighNbr val1, int n)->HighNbr{
	// ------ put variables ------------
        HighNbr reslt; 
	//----------------------------------
		reslt.dim=val1.dim+n;
		
		reslt.tab=(int*)malloc((reslt.dim) * sizeof(int));
	    for(int i=0;i<n;i++){reslt.tab[i]=0;}
		
        for(int i=0;i<val1.dim;i++){
                reslt.tab[i+n]=val1.tab[i];
        };
   
    return reslt;
};*/




#endif /* LONGNOMBRE_H_ */

