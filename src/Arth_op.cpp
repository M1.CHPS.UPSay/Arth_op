/*
!         CONV2B.CPP
!====================================================================
!
!History  T BOUBEHZIZ
!+        14/12/2016
!+        
!+        convert number under any arithmetic basis. 

!History  T BOUBEHZIZ 
!+        07/01/2017
!+        
!+        convert number under 256 arithmetic basis and save 
!         it on HighNbr type.
!
!====================================================================
*/

#include <iostream>

#include<string.h>
#include "OpNum.h"
#include "conv2b.h"
#include "karatsuba.h"
#include "toolsNum.h"
#include <ctime> 
#include <time.h>
using namespace std;

int basis=256;

int main(){

HighNbr x,y;

x.dim=4;
y.dim=3;
x.tab=(int*)calloc(x.dim, sizeof(int));
x.tab[0]=220; 
x.tab[1]=5;
x.tab[2]=13;
x.tab[3]=2;
//---------
y.tab=(int*)calloc(y.dim, sizeof(int));
y.tab[0]=21;
y.tab[1]=93;
y.tab[2]=2;
	

	for(int i=0;i<x.dim;i++)
	cout<< "n "<<i<< " x "<< x.tab[i] <<"\n" ;
	cout<< " ------------------------ \n" ;

	for(int i=0;i<y.dim;i++)
	cout<< "n "<<i<< " y "<< y.tab[i] <<"\n" ;
	cout<< " ------------------------ \n" ;
	
const clock_t begin_time = clock();

auto sol=karatsuba(x,y);

for(int i=0;i<sol.dim;i++)
cout<< "val "<< sol.tab[i] << " 256 ^" << i<<"\n";

std::cout << "time : " <<float( clock () - begin_time ) /  CLOCKS_PER_SEC;

return 0;

}
