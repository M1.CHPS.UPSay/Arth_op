/*
!         CONV2B.CPP
!====================================================================
!
!History  T BOUBEHZIZ
!+        14/12/2016
!+        
!+        convert number under any arithmetic basis. 

!History  T BOUBEHZIZ 
!+        07/01/2017
!+        
!+        convert number under 256 arithmetic basis and save 
!         it on HighNbr type.
!
!====================================================================
*/


#include <cmath>
#include <iostream>
#include "OpNum.h" // file that contains operations under HighNbr type
#include "conv2b.h"
#include "toolsNum.h" // contains manipulation numbers. 
 
using namespace std; 

HighNbr conv2b(int nbr,int n){
	int rest=0,cof1=0,basis=256;
	HighNbr reslt;
	n++;

	if (nbr<basis){
		cout<<"nbr " << nbr << " mod "<< basis << " cof "<< 0 <<" rest " << nbr << " power "<< n<<'\n';
		reslt.dim=n+1;
        reslt.tab=(int*)calloc(reslt.dim, sizeof(int));
		reslt.tab[n]=nbr;
	 return reslt;
	}
    
	cof1= nbr/basis;
	rest=nbr-cof1*basis;
	cout<<"nbr " << nbr << " mod "<< basis << " cof "<< cof1 <<" rest " << rest << " power "<< n<<'\n';
	reslt=conv2b(cof1,n);
	reslt.tab[n]=rest;
    reslt.sig=1;
	return reslt;
};



