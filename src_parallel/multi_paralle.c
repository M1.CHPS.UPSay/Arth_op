#include<stdio.h>
#include<mpi.h>
#include <stdlib.h>
int main(int argc, char ** argv){

//--------------------------------

// MPI :

int rank, total_proc; // proc
// total number of test cases
long long int n_per_proc,dimtotal=4;
int *nbrMax,*nbrMin,nbrMaxdim=4,nbrMindim;
int *accum,*reslt;
int *sum,startval,endval;
reslt=(int*)calloc(nbrMaxdim, sizeof(int));
//-----------------------------------------------
nbrMaxdim=4;
nbrMax=(int*)calloc(nbrMaxdim, sizeof(int));
nbrMax[0]=220; 
nbrMax[1]=5;
nbrMax[2]=13;
nbrMax[3]=2;
//---------
nbrMindim=3;
nbrMin=(int*)calloc(nbrMaxdim, sizeof(int));
nbrMin[0]=21;
nbrMin[1]=93;
nbrMin[2]=2;
//-----------------------------------------------
int jeton, ctp=0,tag1=100,tag2=101,*local_rest;
int *global_rest,*rec_rest;
MPI_Status sta;
MPI_Init(&argc,&argv);
MPI_Comm_size(MPI_COMM_WORLD, &total_proc); // get total_proc
MPI_Comm_rank(MPI_COMM_WORLD, &rank); // get rank

global_rest=(int*)calloc(total_proc,sizeof(int));
rec_rest=(int*)calloc(total_proc,sizeof(int));

n_per_proc = dimtotal/total_proc;
//printf("dimtotal %d , rank %d , n_per_proc %d ",total_proc,rank,n_per_proc);
accum=(int*)calloc(n_per_proc, sizeof(int));
//---------
sum=(int*)calloc(n_per_proc,sizeof(int));


 while(1){
// multiplication elementaire pour chaque proc rank.
for(int i=0,j=rank*n_per_proc;i<n_per_proc;i++){
sum[ctp+i] = sum[ctp+i] + nbrMax[j] * nbrMin[ctp];
if(sum[i]>=256){
				 sum[i]=sum[i]%256; // gestion de retenu intérieur.
                 if(i+1<n_per_proc)
				     reslt[i+1]=reslt[i+1]+1;
		   }
j++;
}

    if(rank == 0) {
        if(ctp<nbrMindim)
        ctp++;
        printf("rank %d ctp %d\n",rank,ctp);
        }   

     MPI_Bcast(&ctp, 1, MPI_INT, 0, MPI_COMM_WORLD);
    // MPI_Barrier(MPI_COMM_WORLD);
    printf("rank %d ctp %d\n",rank,ctp);
    
    if(ctp>=nbrMindim)
      break;

 }

//printf("ctp %d\n",ctp);
//-----------------------------------------------------
// gestion de retenu entre les processeurs.

while(1){
    if(sum[n_per_proc-1]>256){
       sum[n_per_proc-1]=sum[n_per_proc-1]%256;
       global_rest[rank]=1;   
    }  
    MPI_Barrier(MPI_COMM_WORLD);
    if(rank==0)
      break;

    if(global_rest[rank]==0){
        for(int i=0;i<rank;i++){
            if(global_rest[i]==0)
             rec_rest[rank]=1;
         }
    }
    if(rec_rest[rank]==1 && rank>0)
       break;

    if(global_rest[rank-1]!=0 && rank>0){
        sum[0]=sum[0]+1;
        for(int i=0;i<n_per_proc;i++){
                if(sum[i]>=256){
		 	    	 sum[i]=sum[i]%256;
                     if(i+1<n_per_proc)
				         sum[i+1]=sum[i+1]+1;
	            }else
                break;
        }
    }
   
}


//----------------------------------------------------------------------------------

if (rank != 0)
    {   
        MPI_Send(sum, n_per_proc, MPI_INT, 0, tag1, MPI_COMM_WORLD);
    }
    else if (rank == 0)
    {   for(int i=0;i<n_per_proc;i++){
           reslt[i]=sum[i];
        }
        //-------------------------------   
        for(int i=1,k=n_per_proc;i<total_proc;i++){
        MPI_Recv(accum, n_per_proc, MPI_INT, i, tag1, MPI_COMM_WORLD, &sta);
        for(int j=0;j<n_per_proc;j++){
             reslt[k]=reslt[k]+accum[j];
             k++;
           }
        }
    }


MPI_Barrier(MPI_COMM_WORLD);
if(rank==0){
printf("\n result\n");
for(int i=0;i<nbrMaxdim;i++)
printf(" %d\n",reslt[i]);
};
MPI_Finalize();

}